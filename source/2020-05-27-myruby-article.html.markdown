---
title: My Second Article
date: 2020-05-27
tags: example , exploration
---

This is my first article. This blog has been generatedby Meshark using [middleman](https://middlemanapp.com/basics/blogging/).

![My Photo](/blog/images/middleman.png)

The blog post itself is written in [markdown](https://www.markdownguide.org). 

More articles to come.
